TUTORIAL (Fuse Integration Services)
====================================

	How to use ConfigMaps and environment variables in a FIS-SpringBoot project.
	(from scratch, using a text editor, all based on FIS 2.0 and CDK 2.3)


	Summary:
	--------
		1) Create FIS-SpringBoot project
		2) Use ConfigMaps
		3) Use environment variables
		4) Test locally
		5) Deploy and test in OpenShift


	And learn interesting bits like:
	--------------------------------
		- run Spring Boot locally before deploying in OpenShift 
		- use handy openshift client commands


	Prerequisites:
	--------------
		> CDK 2.3 installed and running
		> OpenShift Client installed
		> Environment variables pointing to CDK set
		> Access to Red Hat Maven repositories
		> FIS 2.0 images/templates installed 


	Reference documentation:
	------------------------

		https://access.redhat.com/documentation/en-us/red_hat_jboss_middleware_for_openshift/3/html-single/red_hat_jboss_fuse_integration_services_2.0_for_openshift/



==============================


1) Create Maven project:

	(run command with no tabs/spaces on the left)
	
	mvn org.apache.maven.plugins:maven-archetype-plugin:2.4:generate \
	-DarchetypeCatalog=https://maven.repository.redhat.com/ga/io/fabric8/archetypes/archetypes-catalog/2.2.195.redhat-000004/archetypes-catalog-2.2.195.redhat-000004-archetype-catalog.xml \
	-DarchetypeGroupId=org.jboss.fuse.fis.archetypes \
	-DarchetypeArtifactId=spring-boot-camel-xml-archetype \
	-DarchetypeVersion=2.2.195.redhat-000004


2) Create project in OpenShift:

	oc new-project test1


3) Create a test property in a ConfigMap 

	3.1)
		create
			src/main/fabric8/myconfig.yaml

		with
			kind: ConfigMap
			apiVersion: v1
			metadata:
			  name: myconfig
			data:
			  test.prop.springboot: value from ConfigMap

	3.2)
		create
			src/main/resources/bootstrap.properties

		with
			# Bootstrap settings for ConfigMap tutorial
			spring.application.name=myconfig

		Note the value of property 'spring.application.name' needs to match the ConfigMap's name.


4) Enable Spring Boot to invoke Kubernetes API

	Spring Boot obtains ConfigMap data via the Kubernetes API. We need to include a Fabric8 dependency to enable this mechanism.
	Add POM dependency to allow Spring Boot to inspect and load configMaps.
	As soon as the dependency is defined and deployed, Spring Boot will attempt to interact with the API to initialise.

		update
			pom.xml

		include
		    <dependency>
		      <groupId>io.fabric8</groupId>
		      <artifactId>spring-cloud-kubernetes-core</artifactId>
		    </dependency>

	For Spring Boot to be allowed to invoke the Kubernetes API, access needs to be granted.
	The following command grants 'view' access:

		oc policy add-role-to-user view --serviceaccount=default

	If the above permission is not granted, your pod may throw a message similar to the following:

		"Forbidden!Configured service account doesn't have access. Service account may have been revoked"


5) Define an OpenShift environment variable

	We include a test environment variable in the OpenShift's deployment configuration.

		update
			src/main/fabric8/deployment.yml

		include
			TEST_ENV_VAR

		should look like
			...
		    containers:
	        - 
	          resources:
	          	...
	          env:
	          - name: TEST_ENV_VAR
	            value: "OpenShift environment value"


6) Update Spring XML file

	Now let's include the following code and print out the prop/env values:

		update
			src/main/resources/spring/camel-context.xml
		
		include
			<bean id="testprop" class="java.lang.String">
				<constructor-arg value="${test.prop.springboot}"/>
			</bean>

		include
			<bean id="testenv" class="java.lang.String">
				<constructor-arg value="${TEST_ENV_VAR}"/>
			</bean>

		include (in Camel)
			<log message="simple expr prop: {{test.prop.springboot}}"/>
			<log message="simple expr env:  ${sysenv.TEST_ENV_VAR}"/>
			<log message="spring bean prop: ${bean:testprop.toString}"/>
			<log message="spring bean env:  ${bean:testenv.toString}"/>

	Note the code included shows how to handle env variables and properties both in the Spring region and Camel region.


7) Running locally

	To run locally we need to define a local environment variable.
	Let's run the application locally and see what the output is:

		define:
			export TEST_ENV_VAR='local env value'


	About the ConfigMap property, Spring Boot needs to be able to resolve it.
	The ConfigMap is obviously not available as we haven't deployed the project in OpenShift yet.

	There are 2 options to move forward:
		a) include a local property to simulate the ConfigMap.
		b) deploy the ConfigMap in OpenShift, Spring Boot will then be able to resolve via the Kubernetes API.


	a) Use of local properties file:

		Using local property files allows you to work without the dependency of an OpenShift environment.
		It simulates the engine is provided with ConfigMap properties, but obviously not using real ConfigMaps. 

	a.1)
		update
			src/main/resources/application.properties

		include
			test.prop.springboot = local prop value

		Note the property name matches the one defined in the ConfigMap.


	a.2)
		Let's build and run locally

		command:
			mvn clean spring-boot:run

		output:
			18:59:03.220 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr prop: local prop value
			18:59:03.220 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr env:  local env value
			18:59:03.220 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean prop: local prop value
			18:59:03.221 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean env:  local env value

		You can observe the property value was picked up from the properties file and the env variable resolved.


	b) Deploy the ConfigMap and use locally:

		The second option is to deploy the ConfigMap configuration in OpenShift.
		When running locally, Spring Boot will query OpenShift and obtain the values.

	b.1) 
		Let's install the ConfigMap:
 
		deploy ConfigMap:
			oc create -f src/main/fabric8/myconfig.yaml

	b.2) 
		Let's restart Spring Boot to pick up the changes.

		command:
			mvn spring-boot:run

		output:
			19:01:16.849 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr prop: value from ConfigMap
			19:01:16.849 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr env:  local env value
			19:01:16.849 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean prop: value from ConfigMap
			19:01:16.850 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean env:  local env value

		You can observe the property value was this time obtained from the deployed ConfigMap.


8) Running in OpenShift

	Now, let's deploy in OpenShift and observe what the output is:

		command:
	 		mvn clean fabric8:deploy

		you can view the logs with:
			oc get pods
			oc logs <pod_name>

	 	output:
			17:54:24.327 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr prop: value from ConfigMap
			17:54:24.327 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr env:  OpenShift environment value
			17:54:24.328 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean prop: value from ConfigMap
			17:54:24.328 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean env:  OpenShift environment value

	Note when the same property name is defined in the properties file and in a ConfigMap:
		> deploying in OpenShift and running in a Pod will load the ConfigMap and discard the properties file.

	The OpenShift environment variable has also being picked up from the value defined in the deployment configuration.


	When updating the configmap in OpenShift and starting a new pod, the change should be reflected:

		From the web console, navigate to Resources -> Other Resources.
		Choose 'Config Map' from the drop down list, and then Actions -> Edit YAML.

		update with:
			test.prop.springboot: 'value from ConfigMap updated'

		kill the running pod:
			> oc get pods
			  NAME                                   READY     STATUS      RESTARTS   AGE
			  fis-configmap-springboot-1-ngf48       1/1       Running     0          1d

			> oc delete pod fis-configmap-springboot-1-ngf48
			  pod "fis-configmap-springboot-1-ngf48" deleted

		a new pod kicks in and the output should show:
			18:02:17.591 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr prop: value from ConfigMap updated
			18:02:17.592 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr env:  OpenShift environment value
			18:02:17.592 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean prop: value from ConfigMap updated
			18:02:17.592 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean env:  OpenShift environment value

		you can view the logs with:
			oc get pods
			oc logs <pod_name>


	When updating the environment variable, OpenShift kicks off a new pod with the new env value:

		From the web console, navigate to Applications -> Deployments.
		Select the deployment instance from the entry list and select the tab 'Environment'.

			update
				TEST_ENV_VAR

			with
				OpenShift environment updated

		Openshift recognises the change, spins a new pod and discards the previous one showing:
			18:05:11.131 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr prop: value from ConfigMap updated
			18:05:11.131 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - simple expr env:  OpenShift environment updated
			18:05:11.132 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean prop: value from ConfigMap updated
			18:05:11.132 [Camel (camel) thread #0 - timer://foo] INFO  simple-route - spring bean env:  OpenShift environment updated

		you can view the logs with:
			oc get pods
			oc logs <pod_name>

